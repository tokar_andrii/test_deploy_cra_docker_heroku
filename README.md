# Test deploy react app using gitlab ci/cd, docker, heroku

## Demos:

[demo staging](https://cra-docker-heroku-env-staging.herokuapp.com/)

[demo product](https://cra-docker-heroku-env-product.herokuapp.com/)

## Used recources:

[How do I deploy my code to Heroku using GitLab CI/CD? Sam Barros
](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4)

[GitLab CI/CD examples documentation](https://docs.gitlab.com/ee/ci/examples/README.html)

[GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/)
