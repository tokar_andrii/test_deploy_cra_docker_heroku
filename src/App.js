import React from "react";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Test app with using react
        <span role="img" aria-label="react-logo">
          ⚛️
        </span>
        & docker
      </header>
      <main>some main stuff!!!!💥 All it are at master....Add some tests.</main>
    </div>
  );
}

export default App;
